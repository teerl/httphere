/*
Copyright (c) 2015, Thomas Lingefelt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
Httphere - Serve a directory

    -p (8080) Port to listen to
    -sp (10443) Secure port to listen on
    -d (./) Directory to serve
*/
package main

import (
	"flag"
	"log"
	"os"
	"path/filepath"

	httphere "gitlab.com/teerl/httphere/lib"
)

func main() {
	flag.StringVar(&httphere.Port, "p", "8080", "Port to listen to")
	flag.StringVar(&httphere.Sport, "sp", "10443", "Secure port to listen to")
	dirPath := flag.String("d", "./", "Directory to serve")
	flag.Parse()

	info, err := os.Stat(*dirPath)
	if err != nil {
		log.Fatal(err)
	} else if !info.IsDir() {
		log.Fatal(*dirPath, " isn't a directory")
	}
	httphere.DirPath, _ = filepath.Abs(*dirPath)
	httphere.KeyPem = KeyPem
	httphere.CertPem = CertPem
	httphere.Run(nil)
}
