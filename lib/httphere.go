/*
Copyright (c) 2015, Thomas Lingefelt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
In order to serve on a secure port and have no need for other files you'll need
a key and certificate that'll be embedded into the executalbe.
you'll create one or two .go source files that defines...
    const key_pem = `RSA PRIVATE KEY contents`
    // and...
    const cert_pem = `CERTIFICATE contents`
If you need... two such sources can be automatically created for you using the
Makefile in the httphere/lib directory.

I know this isn't desirable for all usages, but this is just a small starter
library for my own toy servers.
*/
package httphere

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"sort"
	"strings"
	"time"
)

//This is the default path that's served by the default "/" handler.
var DirPath string

//The HTTP port
var Port string

//The HTTPS port
var Sport string

// The RSA PRIVATE KEY text
var KeyPem string

// The CERTIFICATE text
var CertPem string

//These variables are passed to http.HandleFunc unimpeded.
type HandlePair struct {
	Pattern string
	Func    http.HandlerFunc
}

type infoList []os.FileInfo

func (lst infoList) Len() int           { return len(lst) }
func (lst infoList) Swap(i, j int)      { lst[i], lst[j] = lst[j], lst[i] }
func (lst infoList) Less(i, j int) bool { return lst[i].Name() < lst[j].Name() }

func init() {
	Port = "8080"
	Sport = "10433"
	DirPath = "./"
}

func prettyByte(n int64) string {
	num := float64(n)
	unit := 1024.0
	affix := "BKMG"
	affix_c := 0
	for num > unit && affix_c < 3 {
		num /= unit
		affix_c += 1
	}
	return fmt.Sprintf("%0.2f%c", num, affix[affix_c])
}

func serveDir(resp http.ResponseWriter, req *http.Request, dir http.File) {
	if req.URL.Path[len(req.URL.Path)-1] != '/' {
		http.Redirect(resp, req, req.URL.Path+"/", 301)
		return
	}
	dirInfo, _ := dir.Stat()
	if !dirInfo.Mode().IsDir() {
		log.Fatal("Not a dir: ", dirInfo.Name())
	}
	infos, err := dir.Readdir(0)
	if err != nil {
		log.Fatal(err)
		return
	}
	sort.Sort(infoList(infos))

	data := `<!DOCTYPE html>
<html>
<title>%s</title>
<body>
<style>
tr:hover
{
 background-color: #eeeeee;
}
</style>
Directory List: %s<hr />
<table>`
	data = fmt.Sprintf(data, req.URL.Path, req.URL.Path)
	for _, info := range infos {
		var extra string
		link := info.Name()
		linkTxt := link
		if link[0] == '.' {
			continue
		}
		if info.IsDir() {
			link += "/"
			linkTxt = link
		} else if info.Mode().IsRegular() {
			extra = prettyByte(info.Size())
		} else if info.Mode()&os.ModeSymlink != 0 {
			linkTxt += "@"
		}
		data += "<tr><td><a href=\"" + link + "\">" + linkTxt + "</a></td>"
		if len(extra) > 0 {
			data += "<td>" + extra + "</td></tr>"
		}
		data += "\n"
	}
	data += "</table></body></html>"
	http.ServeContent(resp, req, req.URL.Path, time.Now(), strings.NewReader(data))
}

func handleReq(resp http.ResponseWriter, req *http.Request) {
	log.Print(req.URL.Path)

	if path.Base(req.URL.Path)[0] == '.' {
		http.NotFound(resp, req)
		return
	}

	rootDir := http.Dir(DirPath)
	fin, err := rootDir.Open(req.URL.Path)
	if err != nil {
		http.NotFound(resp, req)
		log.Print("Not Found")
		return
	}
	defer fin.Close()

	info, _ := fin.Stat()
	if info.Mode().IsDir() {
		serveDir(resp, req, fin)
	} else if info.Mode().IsRegular() {
		http.ServeContent(resp, req, req.URL.Path, info.ModTime(), fin)
	} else {
		http.NotFound(resp, req)
		log.Print("Not Good")
	}
}

// Serve the Port and Sport
func Serve() <-chan error {
	cherr := make(chan error)
	go func() {
		log.Print("Serving ", DirPath, " on port ", Port)
		cherr <- http.ListenAndServe(":"+Port, nil)
	}()

	if KeyPem != "" && CertPem != "" {
		go func() {
			log.Print("Serving ", DirPath, " on secure port ", Sport)
			tlsconfig := &tls.Config{}
			tlsconfig.Certificates = make([]tls.Certificate, 1)
			var err error
			tlsconfig.Certificates[0], err = tls.X509KeyPair([]byte(CertPem), []byte(KeyPem))
			if err != nil {
				log.Fatal(err)
			}
			server := &http.Server{Addr: ":" + Sport, TLSConfig: tlsconfig}
			cherr <- server.ListenAndServeTLS("", "")
		}()
	}

	return cherr
}

// Set up some handlers/func pairs for http to serve.
// This includes setting the "/" request to serve the DirPath
// directory.
//
// If you don't want this then int the pairs you can set "/" to nil or something else.
func SetFancyHandlers(pairs []HandlePair) {
	http.HandleFunc("/", handleReq)
	for _, pair := range pairs {
		http.HandleFunc(pair.Pattern, pair.Func)
	}
}

// Convienence function that calls SetFancyHandlers and Serve
func Run(handlePairs []HandlePair) {
	SetFancyHandlers(handlePairs)
	log.Fatal(<-Serve())
}
