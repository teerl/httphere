all: key.pem.go cert.pem.go

key.pem:
	openssl genrsa -out $@

key.pem.go: key.pem
	echo "package main" > $@
	echo -n 'const KeyPem = `' >> $@
	cat key.pem >> $@
	echo '`' >> $@

cert.pem.go: key.pem
	echo "package main" > $@
	echo -n 'const CertPem = `' >> $@
	yes XX | openssl req -new -x509 -key key.pem >> $@
	echo '`' >> $@

newkey: rmkey all

rmkey:
	rm -f key.pem.go cert.pem.go key.pem

.PHONY: all newkey rmkey
